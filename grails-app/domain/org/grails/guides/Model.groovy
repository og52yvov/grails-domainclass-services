//package org.grails.guides
//
//class Model {
//    String name
//    static belongTo = [make: Make]
//    static constraints = {
//    }
//
//    String toString(){
//        name
//    }
//}
package org.grails.guides

class Model {
    String name
    Make make // Add this property

    static belongsTo = [make: Make]
    static constraints = {
    }

    String toString(){
        name
    }
}

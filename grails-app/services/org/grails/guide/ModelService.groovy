package org.grails.guide

import grails.gorm.services.Service
import org.grails.guides.Make
import org.grails.guides.Model

@Service(Model)
//interface ModelService {
//    Model save(String name, Make make)
//}
interface ModelService {
    Model save(String name, Make make)
}
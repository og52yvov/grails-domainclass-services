package org.grails.guide

import grails.gorm.services.Service
import org.grails.guides.Make

@Service(Make)
interface MakeService {
    Make save(String name)
}

package org.grails.guide

import grails.gorm.services.Service
import org.grails.guides.Make
import org.grails.guides.Model
import org.grails.guides.Vehicle

@Service(Vehicle)
interface VehicleService {
    Vehicle save(String name, Make make, Model model, Integer year)
}

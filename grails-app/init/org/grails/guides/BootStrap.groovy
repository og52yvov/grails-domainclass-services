package org.grails.guides

import groovy.transform.CompileStatic
import org.grails.guide.MakeService
import org.grails.guide.ModelService
import org.grails.guide.VehicleService

@CompileStatic
class BootStrap {
    MakeService makeService
    ModelService modelService
    VehicleService vehicleService

    def init = { servletContext ->

        Make nissan = makeService.save('Nissan')
        Make ford = makeService.save( 'Ford')

        Model titan = modelService.save('Titan', nissan)
        Model leaf = modelService.save('Leaf', nissan)
        Model windstar = modelService.save('Windstar', ford)

        vehicleService.save('Pickup', nissan, titan, 2012).save()
        vehicleService.save('Economy', nissan, leaf, 2014).save()
        vehicleService.save('Minivan', ford, windstar, 1990).save()

//        Vehicle pickup = vehicleService.save('Pickup', nissan, titan, 2012)
//        pickup.save()
//
//        Vehicle economy = vehicleService.save('Economy', nissan, leaf, 2014)
//        economy.save()
//
//        Vehicle minivan = vehicleService.save('Minivan', ford, windstar, 1990)
//        minivan.save()

    }
    def destroy = {
    }
}